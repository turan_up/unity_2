﻿using UnityEngine;
using System.Collections;

public class Actor : MonoBehaviour
{
	public DamageController damageController;
	public float health = 10f;
	public float damage = 10f;
	public float critMultiplier = 2f;

	private float _health;
	private Body body;

	void Awake () 
	{
		body = GetComponent<Body>();
		_health = health;
	}

	public void SetBeginPos()
	{
		if(body) body.SetBeginPos();

		transform.LookAt(new Vector3());
	}

	public void Reset()
	{
		if(body) body.Reset();

		damageController.Reset();
		_health = health;
	}

	public void SetCheckFunk(UnitController.CheckStart checkStart )
	{
		if(body) body.SetCheckFunk(checkStart);
	}

	public bool IsDeath
	{
		get
		{
			if(_health <= 0) return true;
			else return false;
		}
	}

	public Strike GetStrike()
	{
		if(IsDeath) return new Strike(" ",0);

		float random = Random.value ;
		Strike strike;

		if(random < 0.5f) 
		{
			strike = new Strike(Const.hit , random * damage) ;
			Action(Const.attack);
		}
		else
		{
			strike = new Strike(Const.crit , random * damage * critMultiplier) ;
			Action(Const.attack_2);
		}

		return strike;

	}


	public void StartFatality()
	{
		body.StartFatality();
	}

	public void SetStrike(Strike strike)
	{
		if(IsDeath) return;

		_health -= strike.Damage;

		damageController.ActivateDamage(strike.Damage);

		if(_health <= 0 )
		{
			_health = 0;
			Action(Const.death);
		}
		else
		{
			Action(strike.Type);
		}

	}

	public void Action (string act ) 
	{
		if(body) body.Action(act);
	}
	
}



