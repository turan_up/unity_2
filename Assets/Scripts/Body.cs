﻿using UnityEngine;
using System.Collections;

public class Body : MonoBehaviour
{
	public float speed = 10f;
	public float range;
	public Transform respawn;

	private Transform shadowTarget;
	private Transform shadowPlane;

	private Animator anim;
	private BloodPlaneController bloodController;
	private Vector3 heading;

	private ParticleSystem fx_blood;
	private ParticleSystem fx_supper_attack;

	private Vector3 target = new Vector3();

	private bool needWalk = false;
	private int finalRotation = 90;

	// Use this for initialization
	void Awake () 
	{
		anim = GetComponent<Animator> ();
		bloodController = transform.Find("BloodRiver").GetComponent<BloodPlaneController> ();

		InitParticleSystems();

		InitShadowObj();

		if(BerserkBody) finalRotation = 60;
	}





	public void SetBeginPos()
	{
		transform.position = respawn.position;
		Action(Const.idle);
	}

	// play Actoin 
	public void Action (string action ) 
	{

		if(anim) anim.Play(action);

		switch(action)
		{
			case Const.crit:
			case Const.hit:
				fx_blood.Play();
			break;
			case Const.death:
				fx_blood.Play();
				StartCoroutine(StartBloodRiver());
			break;
			case Const.attack_2:
				fx_supper_attack.Play();
			break;
		}

	}



	// Update is called once per frame
	void Update () 
	{
		SetShadowPos();

		FatalityRotation();

		heading = target - transform.position;

		if(heading.sqrMagnitude > range * range)
		{
			// not yet at target
			transform.Translate(transform.forward * speed * Time.deltaTime * 1 , Space.World);
			if(!needWalk) 
			{
				needWalk = true;
				if(anim) anim.SetBool("Run", needWalk);
			}
		}
		else
		{
			//at target - stop run end call CheckStartFunk
			if(needWalk) 
			{
				needWalk = false;
				if(anim) anim.SetBool("Run", needWalk);

				CheckStartFunk(name);
			}
		}


	}


	UnitController.CheckStart CheckStartFunk;
	public void SetCheckFunk(UnitController.CheckStart checkStart )
	{
		CheckStartFunk = checkStart;
	}

	public void StartFatality()
	{
		StartCoroutine(PlayFatalityRotation());
		//needFatalityRotation = true;
	}


	public void Reset()
	{
		transform.rotation = new Quaternion(0,0,0,0);

		fx_blood.Stop();
		fx_supper_attack.Stop();

		Action(Const.idle);

		bloodController.ResetBloodRiver();

		needFatalityRotation = false;

	}

	private bool needFatalityRotation = false;
	private void FatalityRotation()
	{
		if(needFatalityRotation)
		{
			Quaternion target = Quaternion.Euler (0, finalRotation, 0);
			transform.rotation = Quaternion.Slerp (transform.rotation, target, Time.deltaTime * 2);
		}
	}

	private void SetShadowPos()
	{
		Vector3 shadowPos = shadowTarget.position;
		shadowPos.y = -0.1f;
		shadowPlane.position = shadowPos;
	}

	private  bool OrcBody
	{
		get {return (transform.name == "Orc_defence"); }
	}
	
	private  bool BerserkBody
	{
		get {return (transform.name == "nord_berserk");	}
	}

	private IEnumerator PlayFatalityRotation()
	{
		yield return new WaitForSeconds(3.1f);
		needFatalityRotation = true;
		StartCoroutine(PlayFatality());
	}

	private IEnumerator PlayFatality()
	{
		yield return new WaitForSeconds(2.1f);
		Action(Const.attack_2);
	}

	private IEnumerator StartBloodRiver()
	{
		yield return new WaitForSeconds(2.1f);
		bloodController.ActivateBloodRiver();
	}

	private void InitParticleSystems()
	{
		if(BerserkBody)
		{
			fx_blood = transform.Find("Bone_Berserk_").GetComponentInChildren<ParticleSystem>();
			fx_supper_attack = transform.Find("Eff_Heal_2").GetComponentInChildren<ParticleSystem>();
		}
		else if(OrcBody) 
		{
			fx_blood = transform.Find("Orc_Defence:Root_M").GetComponentInChildren<ParticleSystem>();
			fx_supper_attack = transform.Find("GroundFX_Fire").GetComponentInChildren<ParticleSystem>();
		}
	}
	
	private void InitShadowObj()
	{
		if(BerserkBody) shadowTarget = transform.Find("Bone_Berserk_");
		else if(OrcBody) shadowTarget = transform.Find("Orc_Defence:Root_M");
		
		if(!shadowTarget) shadowTarget = transform;
		
		shadowPlane = transform.Find("Shadow_Plane");
		
	}
	
	
}
