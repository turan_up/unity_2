﻿using UnityEngine;
using System.Collections;

public class DamageController : MonoBehaviour 
{
	public TextMesh textMesh;
	private Animation damageAnim;

	// Use this for initialization
	void Start () 
	{
		damageAnim = gameObject.GetComponent<Animation>();
		gameObject.SetActive(false);
	}

	public void ActivateDamage(int dam)
	{
		textMesh.text = "";
		gameObject.SetActive(true);
		StartCoroutine(AnimateDamage(dam));
	}

	private IEnumerator AnimateDamage(int dam)
	{
		yield return new WaitForSeconds(1.1f);

		textMesh.text = "-" + dam;
		if(damageAnim) damageAnim.Play("d_anim_clip");
	}

	public void Reset()
	{
		gameObject.SetActive(false);
	}

}
