﻿using UnityEngine;
using System.Collections;

public class Strike
{
	public string Type {
		get; 
		private set;
	}
	public int Damage 
	{
		get;
		private set;
	}

	public  Strike (string type, float damage)
	{
		Type = type;
		Damage = (int) damage;

		if(Damage == 0) Damage = 1;
		//Debug.Log(damage + " " + Damage);
	}
	//Other properties, methods, events...
}
