﻿using UnityEngine;
using System.Collections;

public class BloodPlaneController : MonoBehaviour 
{
	//vars for the whole sheet
	public bool loop = true;

	public int colCount =  4;
	public int rowCount =  4;
	
	//vars for animation
	public int  rowNumber  =  0; //Zero Indexed
	public int colNumber = 0; //Zero Indexed
	public int totalCells = 4;
	public int  fps     = 10;

	//Maybe this should be a private var
	private Vector2 offset;
	private Renderer render;

	private bool play = true;

	void Start () 
	{
		render = GetComponent<Renderer>();
		ResetBloodRiver();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(play) SetSpriteAnimation(colCount,rowCount,rowNumber,colNumber,totalCells,fps);  
	}

	float curTime; 
	public void ActivateBloodRiver()
	{
		curTime = Time.time;
		play = true;
	}

	public void ResetBloodRiver()
	{
		play = false;
		render.material.SetTextureScale  ("_MainTex", new Vector2(0,0));
	}

	//SetSpriteAnimation
	void SetSpriteAnimation(int colCount ,int rowCount ,int rowNumber ,int colNumber,int totalCells,int fps ){
		
		// Calculate index
		int index  = (int)((Time.time - curTime )* fps);
		//Debug.Log(index);
		// Repeat when exhausting all cells
		if(loop)	index = index % totalCells;
		else if (index >= totalCells - 1)
		{
			index = totalCells - 1;
			play = false;
		}
		
		// Size of every cell
		float sizeX = 1.0f / colCount;
		float sizeY = 1.0f / rowCount;
		Vector2 size =  new Vector2(sizeX,sizeY);
		
		// split into horizontal and vertical index
		var uIndex = index % colCount;
		var vIndex = index / colCount;
		
		// build offset
		// v coordinate is the bottom of the image in opengl so we need to invert.
		float offsetX = (uIndex+colNumber) * size.x;
		float offsetY = (1.0f - size.y) - (vIndex + rowNumber) * size.y;
		Vector2 offset = new Vector2(offsetX,offsetY);


		render.material.SetTextureOffset ("_MainTex", offset);
		render.material.SetTextureScale  ("_MainTex", size);
	}
}
