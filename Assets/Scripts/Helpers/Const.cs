﻿using UnityEngine;
using System.Collections;

public class Const {

	public const string attack = "attack";
	public const string attack_2 = "attack_2";
	public const string death = "death";
	public const string hit = "hit";
	public const string crit = "crit";
	public const string run = "run";
	public const string idle = "idle";

	public const string Horizontal = "Horizontal";
	public const string Vertical = "Vertical";


}
