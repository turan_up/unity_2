﻿using UnityEngine;
using System.Collections;

public class GUI_Build : MonoBehaviour
{
	public UnitController unitController;
	public Texture2D icon;

	private bool isAndroid = false;
	private Rect restartBtn = new Rect(Screen.width - 110,30,80,80);

	void Start () 
	{
		#if UNITY_ANDROID
		isAndroid = true;
		//restartBtn = new Rect(Screen.width - 150,30,120,120);
		#endif


	}
	void OnGUI () 
	{
		if(isAndroid)
		{
			if(GUI.Button(new Rect(Screen.width - 150,30,120,120), icon)) unitController.Reset();
		}
		else
		{
			if(GUI.Button(restartBtn, icon)) unitController.Reset();
		}

	}
}
