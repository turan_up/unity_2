﻿using UnityEngine;
using System.Collections;

public class UnitController : MonoBehaviour 
{
	private Actor orcActor;
	private Actor berserkActor;

	public delegate  void CheckStart (string actorName);

	void Start () 
	{
		InitUnits();
	}

	private void InitUnits()
	{
		orcActor = GameObject.Find("Orc_defence").GetComponent<Actor>();
		berserkActor = GameObject.Find("nord_berserk").GetComponent<Actor>();

		orcActor.SetCheckFunk(CheckOnStartBattle);
		berserkActor.SetCheckFunk(CheckOnStartBattle);

		SetUnitBeginPos();
	}

	private void SetUnitBeginPos()
	{
		orcActor.SetBeginPos();
		berserkActor.SetBeginPos();
	}


	private bool orcReady = false;
	private bool berserkReady = false;
	private void CheckOnStartBattle(string actorName)
	{
		if(actorName == orcActor.name) orcReady = true;
		else if( actorName == berserkActor.name) berserkReady = true;

		if(orcReady && berserkReady) StartBattle();
	}

	private void StartBattle ()
	{
		needBattleAct = true;
		StartCoroutine("SetBattleAct");
	}


	void Update () 
	{
		//if(Input.GetKeyUp(KeyCode.A)) {OrcAttack();}
		//if(Input.GetKeyUp(KeyCode.D)) {BerserkAttack();}
		//if(Input.GetKeyUp(KeyCode.R)) {Reset();}
	}

	private bool needBattleAct;
	IEnumerator SetBattleAct()
	{
		while(needBattleAct)
		{
			SpotActorAttack();
			CheckWinner();
			yield return new WaitForSeconds(4f);
		}
	}

	private bool orcWin = false;
	private bool berserkWin = false;
	private void CheckWinner()
	{
		if(orcActor.IsDeath) berserkWin = true;
		else if(berserkActor.IsDeath) orcWin = true;

		if(berserkWin) berserkActor.StartFatality();
		else if(orcWin) orcActor.StartFatality();

		if(berserkWin || orcWin) 
		{
			needBattleAct = false;
		}
	}


	private void SpotActorAttack()
	{
		if(Random.value < 0.5f ) OrcAttack();
		else BerserkAttack();
	}


	void OrcAttack()
	{
		if(berserkActor) berserkActor.SetStrike(orcActor.GetStrike());
	}

	void BerserkAttack()
	{
		if(orcActor) orcActor.SetStrike(berserkActor.GetStrike());
	}

	public void Reset()
	{

		if(orcActor) orcActor.Reset();
		if(berserkActor) berserkActor.Reset();

		orcWin = false;
		berserkWin = false;

		orcReady = false;
		berserkReady = false;

		needBattleAct = false;

		SetUnitBeginPos();
	}


}
