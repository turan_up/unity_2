﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
	public float speed = 5f;
	public Rect areaBounds;

	float zoomCount = 0;
	int stepOfZoom = 6;
	float scaleFactor = 5;
	Rect defaultBounds;
	// Use this for initialization
	void Start ()
	{
		defaultBounds = new Rect(areaBounds);
		if(Screen.orientation == ScreenOrientation.LandscapeLeft || Screen.orientation == ScreenOrientation.LandscapeRight) 
		{
			isPort = true;
			transform.Translate(Vector3.forward * scaleFactor);
		}
		else 
		{
			isLand = true;

			float factor = (float) Screen.height / Screen.width;
			areaBounds.y = (int) areaBounds.y / factor;
			areaBounds.height = (int) areaBounds.height / factor;
		}
	}
	
	// Update is called once per frame
	void LateUpdate ()
	{
		//not mobile input
		changeZoom(Input.GetAxis("Mouse ScrollWheel") * 2);
		//if(Input.GetKeyUp(KeyCode.KeypadMinus)) changeZoom(-1);
		//if(Input.GetKeyUp(KeyCode.KeypadPlus)) changeZoom(1);

		twoAxisMove();

		CheckTouch();

		CheckOrientation();
	}

	bool isLand = false;
	bool isPort = false;
	private void CheckOrientation()
	{


		if(Input.deviceOrientation == DeviceOrientation.LandscapeLeft || Input.deviceOrientation == DeviceOrientation.LandscapeRight)
		{
			if(isLand)
			{
				transform.Translate(Vector3.forward * scaleFactor);
				isLand = false;
				isPort = true;

				areaBounds = new Rect(defaultBounds);
			}

		}

		if(Input.deviceOrientation == DeviceOrientation.Portrait || Input.deviceOrientation == DeviceOrientation.PortraitUpsideDown)
		{
			if(isPort)
			{
				transform.Translate(Vector3.forward * -scaleFactor);
				isPort = false;
				isLand = true;

				float factor = (float) Screen.height / Screen.width;
				areaBounds.y = (int) areaBounds.y / factor;
				areaBounds.height = (int) areaBounds.height / factor;
			}
			
		}


	}

	float lastDist;
	float curDist;
	void CheckTouch()
	{
		Touch touch;
		int nbTouches = Input.touchCount;
		
		if(nbTouches == 1)
		{
			touch = Input.GetTouch(0);
			if(touch.phase == TouchPhase.Moved) CameraNewPos(new Vector3(touch.deltaPosition.y/10f,0,-touch.deltaPosition.x / 10f));
		}
		else if (nbTouches == 2)
		{
			Vector2 heading = Input.GetTouch(0).position - Input.GetTouch(1).position;
			curDist = heading.magnitude;
			if(Input.GetTouch(0).phase == TouchPhase.Moved || Input.GetTouch(1).phase == TouchPhase.Moved) changeZoom((curDist-lastDist)/20f);
			lastDist = curDist;
		}
	}

	void changeZoom(float diff)
	{
		if(Mathf.Abs(zoomCount + diff) <= stepOfZoom)
		{
			zoomCount += diff;
			transform.Translate(Vector3.forward * diff);
		}
	}


	void twoAxisMove()
	{
		float transX = -Input.GetAxis(Const.Vertical) * speed;
		float transZ = Input.GetAxis(Const.Horizontal) * speed;
		transX *= Time.deltaTime;
		transZ *= Time.deltaTime;
		//transform.Translate(transX, 0, 0);
		//transform.Translate(0, 0, transZ);
		//transform.position += new Vector3(transX,0,transZ);

		//Vector3 newPos = transform.position + new Vector3(transX,0,transZ);
		CameraNewPos(new Vector3(transX,0,transZ));
	}


	void CameraNewPos(Vector3 changePos)
	{
		Vector3 newPos = transform.position + changePos;
	//	Debug.Log(areaBounds);
	//	Debug.Log(newPos.x + "  " + newPos.z);
		if(areaBounds.Contains(new Vector2(newPos.x,newPos.z)))
		{
			transform.position = newPos;
		}
		else 
		{
			if ( newPos.x <= areaBounds.x ) newPos.x = areaBounds.x;
			if ( newPos.z <= areaBounds.y ) newPos.z = areaBounds.y;
			if ( newPos.x >= areaBounds.x + areaBounds.width ) newPos.x = areaBounds.x + areaBounds.width;
			if ( newPos.z >= areaBounds.y + areaBounds.height ) newPos.z = areaBounds.y + areaBounds.height;
			
			transform.position = newPos;
		}
	}

}
